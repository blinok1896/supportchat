import './App.css';
import React, {useEffect} from 'react'
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./routes/AppRouter";
import './Components/LoginPage/LoginPageStyle/LoginPageStyle.css';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {auth} from "./index";
import {onAuthStateChanged} from 'firebase/auth'
import {useDispatch} from "react-redux";
import {LOGGED_FALSE, LOGGED_TRUE} from "./redux/actions/actions";

function App() {
    const dispatch = useDispatch()
    useEffect(()=> {
        onAuthStateChanged(auth, (user) =>{
            if(user) {
                 dispatch({type: LOGGED_TRUE,payload: user})

            } else {
                dispatch({type: LOGGED_FALSE})
            }
        })
    })
  return <BrowserRouter>
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
      />
      <AppRouter />
  </BrowserRouter>
}
export default App;
