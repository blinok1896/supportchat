import {
    CHAT_ROUTE,
    FORGOT_PASSWORD_ROUTE,
    LOGIN_ROUTE,
    REGISTRATION_ROUTE
} from "./utils";
import MainChat from "../Components/MainChat/MainChat";
import SignUp from "../Components/LoginPage/Components/SignUp/SignUp";
import ForgotPassword from "../Components/LoginPage/Components/ForgotPassword/ForgotPassword";
import LogIn from "../Components/LoginPage/Components/LogIn/LogIn";

export const publicRoutes = [{
    path: LOGIN_ROUTE,
    Component: <LogIn />
},
    {   path: REGISTRATION_ROUTE,
    Component: <SignUp />
},
    {   path: FORGOT_PASSWORD_ROUTE,
        Component: <ForgotPassword />
    }]
export const privateRoutes = [{
    path: CHAT_ROUTE,
    Component: <MainChat />
}]