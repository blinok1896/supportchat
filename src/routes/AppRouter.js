import React from 'react';
import {Route, Routes} from "react-router-dom";
import {privateRoutes, publicRoutes} from "./routes";
import {useSelector} from "react-redux";
import LogIn from "../Components/LoginPage/Components/LogIn/LogIn";
import MainChat from "../Components/MainChat/MainChat";

const AppRouter = () => {
    const userData = useSelector(state => state.user)
    return !userData.isLogged ?
        (
            <Routes>
                {publicRoutes.map(({path, Component}) =>
                    <Route key={path} path={path} element={Component}/>
                )}
                <Route path='*' element={<LogIn/>}/>

            </Routes>
        )
        :
        (
            <Routes>
                {privateRoutes.map(({path, Component}) =>
                    <Route key={path} path={path} element={Component}/>
                )}
                <Route path='*' element={<MainChat/>}/>
            </Routes>
        )
};

export default AppRouter;