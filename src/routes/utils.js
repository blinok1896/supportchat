export const LOGIN_ROUTE = '/login'
export const CHAT_ROUTE = '/chat'
export const REGISTRATION_ROUTE = '/sign-up'
export const FORGOT_PASSWORD_ROUTE = '/forgot-password'