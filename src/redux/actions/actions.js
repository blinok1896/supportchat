export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST'
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS'
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE'
export const FETCH_MESSAGES_REQUEST_SIGN_UP = 'FETCH_MESSAGES_REQUEST_SIGN_UP'
export const FORGOT_PASS = 'FORGOT_PASS'

export const SHOW_DETAILS = 'SHOW_DETAILS'
export const PUSH_ME_MESSAGE = (operator,userText) => {
   return {
       type: 'PUSH_ME_MESSAGE',
       operator,
       userText
   }
}
export const SIGN_OUT = 'SIGN_OUT';
export const LOGGED_TRUE = 'LOGGED_TRUE';
export const LOGGED_FALSE= 'LOGGED_FALSE';