import {all} from 'redux-saga/effects'
import watchRequestSaga from "./requestAuth";
import signUpWatcher from "./requestSignUp";
import forgotPassWatcher from "./requestForgotPass";



export default function* rootSaga() {

    yield all([
        watchRequestSaga(),
        signUpWatcher(),
        forgotPassWatcher()
    ])

}
