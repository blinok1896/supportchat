import {put, takeLatest} from "redux-saga/effects";
import {FETCH_MESSAGES_FAILURE, FETCH_MESSAGES_REQUEST_SIGN_UP} from "../actions/actions";
import {auth} from "../../index";
import {createUserWithEmailAndPassword} from 'firebase/auth'
import {toast} from "react-toastify";


function* signUpWorker(data) {
    const {email,password} = data.payload
    console.log(data.payload)
    try{
        yield createUserWithEmailAndPassword(auth,email,password).then(() => {
            toast('You have successfuly sign up, use your Login and Email to authentication in our app')
        })
    }
    catch (error) {
        yield put({type: FETCH_MESSAGES_FAILURE, payload: error})
        yield toast(`${error}`)
    }
}

export default function* signUpWatcher() {
    yield takeLatest(FETCH_MESSAGES_REQUEST_SIGN_UP,signUpWorker)
}