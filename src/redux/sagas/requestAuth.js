import {takeLatest, put} from 'redux-saga/effects'
import {FETCH_MESSAGES_REQUEST, FETCH_MESSAGES_SUCCESS, FETCH_MESSAGES_FAILURE} from "../actions/actions";
import {signInWithEmailAndPassword} from 'firebase/auth'
import {auth} from "../../index";
import {toast} from "react-toastify";
import {setPersistence, browserSessionPersistence} from 'firebase/auth'

export function* workerRequestSaga(user) {
    const {email, password} = user.payload
const authFunc = () => {
    return setPersistence(auth,browserSessionPersistence).then(
        () => signInWithEmailAndPassword(auth, email, password)
    )
        .catch((error) => {
            return toast(`${error.message}`)
        })
    }
    try {
        const data = yield authFunc()
        yield put({type: FETCH_MESSAGES_SUCCESS, payload: data.user})
        yield toast('You have successfully completed authentication')
    } catch (error) {
        yield put({type: FETCH_MESSAGES_FAILURE, payload: error})
        yield toast(`${error}`)
    }


}

export default function* watchRequestSaga() {
    yield takeLatest(FETCH_MESSAGES_REQUEST, workerRequestSaga)

}