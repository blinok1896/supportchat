import {auth} from "../../index";
import {put, takeLatest} from "redux-saga/effects";
import {FETCH_MESSAGES_FAILURE, FORGOT_PASS} from "../actions/actions";
import {sendPasswordResetEmail} from 'firebase/auth'
import {toast} from "react-toastify";

function* forgotPassWorker(data) {
    const email = data.payload.email
    try{
        yield sendPasswordResetEmail(auth,email).then(()=> {
            toast('Please check your inbox for a confirmation email')
        })
    }
    catch (error) {
        yield put({type: FETCH_MESSAGES_FAILURE, payload: error})
        toast(`${error}`)
    }
}

export default function* forgotPassWatcher() {
    yield takeLatest(FORGOT_PASS,forgotPassWorker)
}