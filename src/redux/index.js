import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootSaga from "./sagas";
import AllReducer from "./AllReducer";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    AllReducer,

    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store