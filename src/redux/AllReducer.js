import {combineReducers} from "redux";
import userDataReducer from "./reducers/reducers";
import listOfRequestReducer from "./reducers/listOfRequestReducer";

const AllReducer = combineReducers({
    user:userDataReducer,
    list:listOfRequestReducer
})
export default AllReducer