import {SHOW_DETAILS} from "../actions/actions";

const initial = [{
    id: 1,
    name: 'Elizabeth Nelson',
    text: [{
        time: '',
        text: 'Go ahead, make my day'
    }],
    show: false,
    chatState: 'active'
}, {
    id: 2,
    name: 'Will Smith',
    text: [{
        time: '',
        text: 'The only thing that I see that is distinctly different about me is I am not afraid to die on a treadmill.'
    }],
    show: false,
    chatState: 'complete'
}, {
    id: 3,
    name: 'Leonardo DiCapri',
    text: [{
        time: '',
        text: 'I am not the kind of person who tries to be cool or trendy, I am definitely an individual'
    }],
    show: false,
    chatState: 'save'
}]

export default function listOfRequestReducer(state = initial, action) {
    switch (action.type) {
        case SHOW_DETAILS : {
            let copy = {
                ...action.payload,
                show: !action.payload.show
            }
            return state.map((data) => {
                let copyFalse = {
                    ...data,
                    show: false
                }
                return data.id === action.payload.id ? copy : copyFalse;
            })
        }
        case 'PUSH_ME_MESSAGE' : {
            const element = {
                time: '',
                userText: action.userText
            }
            action.operator.text.push(element)

            return state.map((data)=> data.id === action.operator.id ? action.operator : data)
        }
        default :
            return state

    }
}