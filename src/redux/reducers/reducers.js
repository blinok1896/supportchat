import {
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS,
    LOGGED_TRUE, LOGGED_FALSE, SIGN_OUT
} from "../actions/actions";

const initial = {
    isLoading: false,
    isLogged: false,
    error: null,
    user: null
}

export default function userDataReducer(state = initial, action) {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST  : {
            return {
                ...state,
                isLoading: true,
                data: false,
                error: null,
                user: action.payload
            }
        }
        case FETCH_MESSAGES_SUCCESS : {
            return {
                ...state,
                isLoading: false,
                isLogged: true,
                error: null,
                user: action.payload
            }
        }
        case FETCH_MESSAGES_FAILURE : {
            return {
                ...state,
                isLoading: false,
                isLogged: false,
                error: action.payload.message,
                user: null
            }
        }
        case SIGN_OUT : {
            return {
                ...state,
                isLoading: false,
                isLogged: false,
                error: null,
                user: null
            }
        }
        case LOGGED_TRUE : {
            return {
                ...state,
                isLogged: true,
                user:action.payload
            }
        }

        case LOGGED_FALSE : {
            return {
                ...state,
                isLogged: false
            }
        }

        default:
            return state
    }


}