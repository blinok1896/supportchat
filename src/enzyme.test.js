import React from "react";
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Enzyme, {shallow} from 'enzyme'
import userDataReducer from "./redux/reducers/reducers";
import App from "./App";
import LogIn from "./Components/LoginPage/Components/LogIn/LogIn";




Enzyme.configure({ adapter: new Adapter() });

    describe('should render LoginPage',() => {
        const setUp = props => shallow(<App {...props} />)
        it('should contain .logPageForm wrapper', ()=> {
            const wrapper = setUp();
            const data = wrapper.find(<LogIn />)
            expect(data).toEqual({});

        });
    })






test('should return the initial state', () => {

    expect(userDataReducer(undefined,{})).toEqual(
        {
            isLoading: false,
            isLogged: false,
            error: null,
            user: null

        }
    )
})