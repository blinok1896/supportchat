import React from 'react';
import { useFormik } from "formik";
import {FETCH_MESSAGES_FAILURE, FORGOT_PASS} from "../../../../redux/actions/actions";
import {Button, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import bg from "../../image/bg.jpg";
import {CHAT_ROUTE, LOGIN_ROUTE} from "../../../../routes/utils";
import {useNavigate} from "react-router-dom";

const ForgotPassword = () => {
    const userData = useSelector(state=>state)
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const formik = useFormik({
        initialValues: {
            email: ''

        },
        validate: values => {
            const errors = {};
            if (!values.email) {
                errors.email = 'Required';
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
                errors.email = 'Invalid email address';
            }
            return errors;
        },
        validateOnChange: false,
        validateOnBlur: false,
        onSubmit: () => {
            formik.errors.email ? dispatch({type: FETCH_MESSAGES_FAILURE}) :
                dispatch({
                    type: FORGOT_PASS, payload: {
                        email: formik.values.email
                    }
                })
            navigate(CHAT_ROUTE)
        },
    });

    return (<div className='login-page'>
        <img src={bg} alt='background' className='login-page__bg' width='100%' height='100%'/>
        <form className='login-form' onSubmit={formik.handleSubmit}>
            <h2>Reset Password</h2>
            <TextField
                id="email"
                name="email"
                type="email"
                label='Email:'
                onChange={formik.handleChange}
                value={formik.values.email}
                helperText={formik.touched.email && formik.errors.email}
            />
            <p className='errs'>{userData.error}</p>
            <Button type="submit" disabled={userData.isLoading}>
                Submit
            </Button>
            <Button onClick={() => navigate(LOGIN_ROUTE)}>back</Button>
        </form>
        </div>
    );
};

export default ForgotPassword;