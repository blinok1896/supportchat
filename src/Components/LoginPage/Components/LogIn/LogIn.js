import React from 'react';
import {Button, TextField} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGoogle, faVk} from "@fortawesome/free-brands-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {useFormik} from "formik";
import {
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS
} from "../../../../redux/actions/actions";
import {Link, useNavigate} from "react-router-dom";
import {CHAT_ROUTE, FORGOT_PASSWORD_ROUTE, REGISTRATION_ROUTE} from "../../../../routes/utils";
import bg from "../../image/bg.jpg";
import styled from 'styled-components';
import {GoogleAuthProvider, signInWithPopup} from "firebase/auth";
import {auth} from "../../../../index";

const LogIn = () => {
    const userData = useSelector(state => state.user)
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const MyLink = styled(Link)`
      color: black;
      text-decoration: none;
      list-style: none;
    `
    //<Formik>
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validate: values => {
            const errors = {};
            if (!values.email) {
                errors.email = 'Required';
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
                errors.email = 'Invalid email address';
            } else if (!values.password) {
                errors.password = 'Required';
            }
            return errors;
        },
        validateOnChange: false,
        validateOnBlur: false,
        onSubmit: () => {
            formik.errors.email || formik.errors.password ? dispatch({type: FETCH_MESSAGES_FAILURE}) :
                dispatch({
                    type: FETCH_MESSAGES_REQUEST, payload: {
                        email: formik.values.email, password: formik.values.password
                    }
                })
            navigate(CHAT_ROUTE)
        },
    });
    //</Formik>
    const googleFunc = () => {
        const provider = new GoogleAuthProvider();

        return signInWithPopup(auth, provider)
            .then((result) => {
                const user = result.user;
                dispatch({type: FETCH_MESSAGES_SUCCESS, payload: user})
            }).catch((error) => {

                dispatch({type: FETCH_MESSAGES_FAILURE, payload: error})
            })
    }
    const VkSession = (object) => {
        try {
            console.log(object)
            dispatch({type: FETCH_MESSAGES_SUCCESS, payload: object.session})
        } catch (error) {
            dispatch({type: FETCH_MESSAGES_FAILURE, payload: error})
        }
    }
    return <div className='login-page'>
        <img src={bg} alt='background' className='login-page__bg' width='100%' height='100%'/>
        <form className='login-form' onSubmit={formik.handleSubmit}>
            <h2>LOGIN</h2>
            <TextField
                id="email"
                name="email"
                type="email"
                label='Email:'
                onChange={formik.handleChange}
                value={formik.values.email}
                helperText={formik.touched.email && formik.errors.email}
            />
            <TextField
                id="password"
                name="password"
                type="password"
                label='Password:'
                onChange={formik.handleChange}
                value={formik.values.password}
                helperText={formik.touched.password && formik.errors.password}
            />
            <p className='errs'>{userData.error}</p>
            <Button type="submit" disabled={userData.isLoading}>
                Submit
            </Button>
            <div className='button-block'>
                <FontAwesomeIcon icon={faGoogle} onClick={googleFunc} cursor='pointer'/>
                <FontAwesomeIcon icon={faVk} cursor='pointer' onClick={() => {
                    return window.VK.Auth.login(VkSession, +4194304)
                }
                }/>
            </div>
            <div className='login-form__links'>
                <MyLink to={REGISTRATION_ROUTE}>Sign up</MyLink>
                <MyLink to={FORGOT_PASSWORD_ROUTE}>Forgot password?</MyLink>
            </div>

        </form>
    </div>


};

export default LogIn;