import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useFormik} from "formik";
import * as Yup from 'yup';
import {
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST_SIGN_UP
} from "../../../../redux/actions/actions";
import bg from "../../image/bg.jpg";
import {Button, TextField} from "@material-ui/core";
import {CHAT_ROUTE, LOGIN_ROUTE} from "../../../../routes/utils";
import {useNavigate} from "react-router-dom";

const SignUpSchema = Yup.object({
    email: Yup
        .string()
        .required('Required'),
    password: Yup
        .string()
        .required('Please Enter your password')
        .matches(
            // eslint-disable-next-line
            "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/",
            "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
        )
});


const SignUp = () => {
    const userData = useSelector(state => state.user);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    //<Formik>
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: SignUpSchema,
        validateOnChange: false,
        validateOnBlur: false,
        onSubmit: () => {
            formik.errors.email || formik.errors.password ? dispatch({type: FETCH_MESSAGES_FAILURE}) :
                dispatch({
                    type: FETCH_MESSAGES_REQUEST_SIGN_UP, payload: {
                        email: formik.values.email, password: formik.values.password
                    }
                })
            navigate(CHAT_ROUTE)
        },
    });

    //</Formik>
    return <div className='login-page'>
        <img src={bg} alt='background' className='login-page__bg' width='100%' height='100%'/>
        <form className='login-form' onSubmit={formik.handleSubmit}>
            <h2>SIGN-UP</h2>
            <TextField
                id="email"
                name="email"
                type="email"
                label='Email:'
                onChange={formik.handleChange}
                value={formik.values.email}
                helperText={formik.touched.email && formik.errors.email}
            />
            <TextField
                id="password"
                name="password"
                type="password"
                label='Password:'
                pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,12}$"
                onChange={formik.handleChange}
                value={formik.values.password}
                helperText={formik.touched.password && formik.errors.password}
            />
            <p className='errs'>{userData.error}</p>
            <Button type="submit" disabled={userData.isLoading}>
                Submit
            </Button>
            <Button onClick={() => navigate(LOGIN_ROUTE)
            }>back</Button>
        </form>
    </div>
};

export default SignUp;