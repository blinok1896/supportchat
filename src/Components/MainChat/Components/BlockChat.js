import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";


const BlockChat = (props) => {
    const userInfo = props.props
    return (
        <div className={'MainChat__Chat'}>
            {userInfo.text.map((data, index) => {
                return <div className={data.userText ? 'currentUser BlockChat' : 'BlockChat'} key={index}>
                    <div className={'imgTime-icon'}>
                        <FontAwesomeIcon icon={faUser} size='xl'/>
                        <p>{data.time}</p>
                    </div>
                    <div className={'message'}>{data.userText ? data.userText : data.text}</div>

                </div>

            })}

        </div>
    );
};

export default BlockChat;