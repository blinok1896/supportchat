import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {SHOW_DETAILS} from "../../../redux/actions/actions";

const ActiveChat = (props) => {
    const list = useSelector(state=>state.list)
    const stateValue = props.chatState
    const dispatch = useDispatch();
    const listOfChat = list.filter((data) => data.chatState === stateValue)

    return (
        <div className='Active-chat'>
            {listOfChat.length >= 1 ? listOfChat.map((person) => {
               return <div className={person.show ? 'chat-person active-click': 'chat-person'} key={person.id} onClick={() => {
                       return dispatch({type:SHOW_DETAILS, payload: person})}
                   }>
                       <span><FontAwesomeIcon icon={faUser} size='2xl'/></span>
                       <div className='chat-person__info'>
                           <span>{person.name}</span>
                           <p>{person.text.map((data) => data.text)}</p>
                       </div>
                       {/*<div className='newMess-icon'>{index}</div>*/}
                   </div>

            }

            ) : <p className={'Active-chat__noChat'}>Now you don't have a {stateValue} conversations</p> }</div>
    );
};

export default ActiveChat;