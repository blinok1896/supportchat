import React, {useState} from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBookmark, faClockRotateLeft, faList, faUser} from "@fortawesome/free-solid-svg-icons";
import Badge from "@material-ui/core/Badge";
import ActiveChat from "./ActiveChat";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "@material-ui/core";
import UserProfile from "./userProfile";
import {auth} from "../../../index";
import {signOut} from 'firebase/auth'
import {toast} from "react-toastify";
import {SIGN_OUT} from "../../../redux/actions/actions";


const ChatBar = () => {
    const options = ['first-options', 'second-options', 'third-options']
    const dispatch = useDispatch()
    const userData = useSelector(state => state.user)
    const list = useSelector(state => state.list)
    const chatComponent = [<ActiveChat chatState={'active'}/>,
        <ActiveChat chatState={'complete'}/>, <ActiveChat chatState={'save'}/>]
    const [MenuPoint, setMenuPoint] = useState(0)
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const numberOfActiveChat = list.filter((data) => data.chatState === 'active').length
    const ITEM_HEIGHT = 48
    const logOutFunc = () => {
        signOut(auth).then(() => {
            toast('You have successfully Signed Out')
            dispatch({type: SIGN_OUT})
        })
    }
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className='ChatBar'>
            <div className='ChatBar__name'>
                <p>WESUPPORT</p>
                <IconButton
                    aria-label="more"
                    aria-controls="long-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                >
                    <MoreVertIcon/>
                </IconButton>
                <Menu
                    id="long-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    PaperProps={{
                        style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                            width: '20ch',
                        },
                    }}
                >
                    {options.map((option) => (
                        <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
                            {option}
                        </MenuItem>
                    ))}
                </Menu>
            </div>
            <div className='ChatBar__icon'>
                <IconButton size="small" className='icon-button' onClick={() => {
                    setMenuPoint(0)
                }}>
                    <Badge badgeContent={numberOfActiveChat} color='primary'>
                        <FontAwesomeIcon icon={faClockRotateLeft} cursor='pointer'/>
                    </Badge>

                </IconButton>
                <IconButton size="small" className='icon-button' onClick={() => {
                    setMenuPoint(1)
                }}>
                    <FontAwesomeIcon icon={faList} cursor='pointer'/>
                </IconButton>
                <IconButton size="small" className='icon-button' onClick={() => {
                    setMenuPoint(2)
                }}>
                    <FontAwesomeIcon icon={faBookmark} cursor='pointer'/>
                </IconButton>
            </div>
            {chatComponent.map((component, index) => {
                return <div key={index}>
                    {index === MenuPoint && <div>{component}</div>}
                </div>
            })}
            <div className={'user-Profile'}>
                <div className={'user-Profile__logo'} onClick={UserProfile}>
                    <span><FontAwesomeIcon icon={faUser} size='2xl'/></span>
                    <p>{userData.user.displayName ? userData.user.displayName : userData.user.email}</p>
                    <p>{userData.user.user?.first_name ? userData.user.user.first_name : ''}</p>
                </div>
                <Button onClick={logOutFunc}>sign out</Button>
            </div>
        </div>
    );
};

export default ChatBar;