import React, {useState} from 'react';
import ChatBar from "./Components/ChatBar";
import './MainChatStyle/MainChatStyle.css'
import {TextField} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFaceSmileBeam} from "@fortawesome/free-regular-svg-icons";
import {faLocationArrow, faPaperclip} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import BlockChat from "./Components/BlockChat";
import {PUSH_ME_MESSAGE} from "../../redux/actions/actions";


const MainChat = () => {
    const list = useSelector(state => state.list)
    const dispatch = useDispatch()
    const [userText, setUserText] = useState('')
    const [errors, setErrors] = useState('')
    const filterList = list.filter((data) => data.show)
    return (
        <div className={'container'}>
            <ChatBar/>
            {filterList.map((data) => {
                return <div className='MainChat' key={data.id}>

                    <div className='MainChat__header'>
                        <div className={'icon-status'}> </div>
                        <p>{data.name}</p>
                    </div>
                    <BlockChat props={data}/>

                    <div className={'MainChat__Footer'}>
                         <span>
                     <TextField
                         placeholder={'Type your message here...'}
                         fullWidth={true}
                         onChange={(e) => setUserText(e.target.value)}
                         helperText={errors}
                     />
                </span>
                        <div className={'button-block'}>
                            <FontAwesomeIcon icon={faFaceSmileBeam} size={'sm'} cursor='pointer'/>
                            <FontAwesomeIcon icon={faPaperclip} size={'sm'} cursor='pointer'/>
                        </div>
                        <div className='button-block__send'
                             onClick={() => {
                                 if (userText.length === 0) {
                                     setErrors('Required')
                                 } else {
                                     setErrors('')
                                     return dispatch(PUSH_ME_MESSAGE(data, userText))
                                 }
                             }}>
                            <FontAwesomeIcon icon={faLocationArrow} size={'sm'} color='white'/>
                        </div>
                    </div>

                </div>
            })}
            {filterList.length < 1 && <div className='startChat'>Start chatting with someone</div>}
        </div>

    );
};

export default MainChat;