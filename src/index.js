import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import store from "./redux";
import {getAuth} from 'firebase/auth'
import { initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyBEJzfeCY3dCA-yPLACqjCCOJ5Br_s6CJ4",
    authDomain: "supportchat-c9c0a.firebaseapp.com",
    projectId: "supportchat-c9c0a",
    storageBucket: "supportchat-c9c0a.appspot.com",
    messagingSenderId: "11876543644",
    appId: "1:11876543644:web:80e5cb41cd46779a065159",
    measurementId: "G-J7SJJPJ3QC"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)

ReactDOM.render(
<React.StrictMode>
    <React.StrictMode>
          <Provider store={store}>
              <App />
          </Provider>
    </React.StrictMode>
</React.StrictMode>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
